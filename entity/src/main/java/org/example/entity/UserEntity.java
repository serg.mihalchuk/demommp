package org.example.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class UserEntity {

    @Id
    private Long id;
    private String firstName;
    private String lastName;
    private String emailAddress;

    @EqualsAndHashCode.Exclude
    private String fullName;

}
